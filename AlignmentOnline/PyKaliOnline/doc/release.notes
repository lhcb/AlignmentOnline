!-----------------------------------------------------------------------------
! Package     : AlignmentOnline/PyKaliOnline
! Responsible : Daria Savrina
! Purpose     : Online calibration of the ECAL with neutral pions
!-----------------------------------------------------------------------------

! 2018-03-11 - Jean-Francois Marchand
 - Code modified to use Pi0Calibration package for fits and calibration

! 2016-06-16 - Daria Savrina
 - updated the tags and software versions for the 2016 data

! 2016-04-24 - Daria Savrina
 - The fitted histograms are now stored at each iteration
 - Added a piece of J-F code to show the pi0 mass distribution
   in each cell

! 2016-03-23 - Daria Savrina
 - corrected the Iterator for the offline tests to 
   set the appropriate directory for the DST, fmDST and root files
 - added a possibility of multiprocess fmDST production
   (MultiKaliPi0_producefmDST.py)
 - corrected a typo in the MergeHistos.py:
   histos.read -> histos.updateFromDB
 - changed the procedure of the histograms merging. Now it is run
   first time from the Analysers (groups of histograms are merged in
   parallel) and then the output from each group is merged in one
   file by the Iterator.

!========= AlignmentOnline/PyKaliOnline v1r1 - 2016-03-01 ===========

! 2016-02-03 - Daria Savrina
 - added some scripts to python/PyKaliOnline/offline_tests
   necessry for offline tests

! 2015-12-11 - Daria Savrina
 - first commit of the package

#!/bin/python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## ============= the basic import ====================
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
import Configurables as Configs
import Gaudi.Configuration as Gaudi

import os, sys, socket, re
from Paths import Paths, importOnline

MSG_VERBOSE = 1
MSG_DEBUG = 2
MSG_INFO = 3
MSG_WARNING = 4
MSG_ERROR = 5
MSG_FATAL = 6
MSG_ALWAYS = 7

import argparse
parser = argparse.ArgumentParser(description="Input file delivery")
parser.add_argument("--index", type=int, dest="index", default=None)
parser.add_argument("--datadir", type=str, dest="datadir", default=None)
parser.add_argument("--workdir", type=str, dest="workdir", default=None)
parser.add_argument("--iteration", type=int, dest="iteration", default=None)
parser.add_argument("--secondPass", type=int, dest="secondPass", default=None)
args = parser.parse_args()

index = args.index
hn = socket.gethostname()
pt = Paths(index, hn, args.workdir, args.datadir)
directory = pt.dst_dir
niter = args.iteration
secondPass = args.secondPass

if secondPass == 1:
    mmap_file = os.path.join(directory, pt.fileprefix()) + '_SecondPass.MMap'
    histo_file = os.path.join(directory,
                              pt.fileprefix()) + '_SecondPass_histo.root'
else:
    mmap_file = os.path.join(directory, pt.fileprefix()) + '_FirstPass.MMap'
    histo_file = os.path.join(directory,
                              pt.fileprefix()) + '_FirstPass_histo.root'

tuplename = "KaliPi0/Pi0-Tuple"
year = '2018'

if '__main__' == __name__:

    from Gaudi.Configuration import *
    from Configurables import LHCbApp
    LHCbApp().DataType = year
    LHCbApp().EvtMax = -1

    from Configurables import CondDBAccessSvc, CondDB, UpdateManagerSvc
    CondDB().UseOracle = False
    CondDB().UseLatestTags = [year]

    from GaudiPython import *
    ApplicationMgr(OutputLevel=INFO, AppName="Pi0MMap2Histo")

    from Configurables import Pi0MMap2Histo
    pi02Histo = Pi0MMap2Histo("Pi0MMap2Histo")
    pi02Histo.nworker = 1
    pi02Histo.filenames = [mmap_file]

    pi02Histo.outputDir = '/group/calo/CalibWork/Histos'
    if secondPass == 1:
        pi02Histo.outputName = pt.fileprefix(
        ) + '_default3cells_SecondPassFit%d' % niter
    else:
        pi02Histo.outputName = pt.fileprefix(
        ) + '_default3cells_FirstPassFit%d' % niter
    if (niter > 1):
        pi02Histo.lambdaFileName = "/group/calo/CalibWork/Histos/%d/lambda.txt" % (
            niter - 1)
        pi02Histo.OutputLevel = ERROR

    from Configurables import GaudiSequencer
    mainSeq = GaudiSequencer("MainSeq")
    mainSeq.Members = [pi02Histo]
    ApplicationMgr().TopAlg.append(mainSeq)
    AppMgr().run(1)

#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# $Id: KaliPi0_produceNtupleStep2.py$
# =============================================================================
## ============= the basic import ====================
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
import Configurables as Configs
import Gaudi.Configuration as Gaudi

import os, sys, socket, re
from Paths import Paths, importOnline

MSG_VERBOSE = 1
MSG_DEBUG = 2
MSG_INFO = 3
MSG_WARNING = 4
MSG_ERROR = 5
MSG_FATAL = 6
MSG_ALWAYS = 7

import argparse
parser = argparse.ArgumentParser(description="Input file delivery")
parser.add_argument("--index", type=int, dest="index", default=None)
parser.add_argument("--datadir", type=str, dest="datadir", default=None)
parser.add_argument("--workdir", type=str, dest="workdir", default=None)
parser.add_argument("--secondPass", type=int, dest="secondPass", default=None)
args = parser.parse_args()

index = args.index
hn = socket.gethostname()
pt = Paths(index, hn, args.workdir, args.datadir)
directory = pt.dst_dir
secondPass = args.secondPass

print "secondPass= ", secondPass

#Calibration Sept 2017
#listFills = [6030,6031,6035,6041,6044,6046,6048,6050,6052,6053,6054,6055,6057,6060,6061,6072,6079,6082,6084,6086,6089,6090,6091,6093,6094,6096,6097,6098,6104,6105,6106,6110,6114,6116,6119,6123,6136,6138,6140,6141,6142,6143,6146,6147,6152,6155,6156,6158,6159,6160,6161,6165,6167,6168,6169,6170,6171,6174,6175,6176,6177,6179,6180,6182,6185,6186,6189,6191,6192,6193]

#Calibration Oct 2017
#6236,6238,6239,6240,6241,6243,6245#---Not added because PMT recovering
#listFills = [6247,6252,6253,6255,6258,6259,6261,6262,6263,6266,6268,6269,6271,6272,6275,6276,6278,6279,6283,6284,6285,6287,6288,6291,6295,6297,6298,6300,6303,6304,6305,6306,6307,6308,6309,6311,6312,6313,6314,6315,6317,6318,6323,6324,6325]

#Test March 2017
#listFills = [6337,6341,6347,6348,6349,6355,6356,6358,6360]

# Test beginning 2018
#listFills = [6574]

# First calibration 2018-04-23
#listFills = [206404]

### Getting the list of fills from the automatic tool
file = open('/group/online/alignment/Calo/CaloStep2Fills.txt', 'r')
fillsList = file.readlines()
listFills = []
for fill in fillsList:
    print fill.split('\n')[0]
    listFills.append(int(fill.split('\n')[0]))
print listFills

# # Remove list of fills I do not want to process
# Fills just after technical stop and high beta* of July 2018
# badFills = range(6843, 6901)
# Fill 7110 that had problem with HV auto-update
# badFills = [7110, 7120]
# Fills just after technical stop of September 2018
# badFills = [7211, 7212, 7213, 7217]
# for fill in badFills:
#    if fill in listFills:
#         listFills.remove(fill)

if secondPass == 1:
    tup_file = os.path.join(directory, pt.fileprefix()) + '_SecondPass.root'
    #histo_file   = os.path.join(directory, pt.fileprefix())+'_HistosSecondPass.root'
    dstname = os.path.join(directory, pt.fileprefix())
else:
    tup_file = os.path.join(directory, pt.fileprefix()) + '_FirstPass.root'
    #histo_file   = os.path.join(directory, pt.fileprefix())+'_HistosFirstPass.root'
    dstname = os.path.join(directory, pt.fileprefix())

from Configurables import OffLineCaloRecoConf
from Configurables import OffLineCaloPIDsConf

OffLineCaloRecoConf(
    EnableRecoOnDemand=True,
    UseTracks=True,  #False ,         ## do not use tracks!
    UseSpd=True,  #False ,
    #UsePrs             = False ,
    ForceDigits=False,
    MeasureTime=True,
    OutputLevel=ERROR)

OffLineCaloPIDsConf(
    EnablePIDsOnDemand=True, MeasureTime=True, OutputLevel=ERROR)

from GlobalReco.GlobalRecoConf import NeutralProtoPAlg
proto = NeutralProtoPAlg(
    "NeutralProtoPMaker",
    LightMode=True,  ## "ligght-mode", no PIDs
    HyposLocations=['Rec/Calo/Photons'],  ## only single photons
    OutputLevel=ERROR)

from ParticleMaker.ParticleMakerConf import PhotonMakerAlg, PhotonMaker
maker = PhotonMakerAlg(
    'StdLooseAllPhotons', OutputLevel=ERROR, DecayDescriptor='Gamma')
maker.addTool(PhotonMaker, name='PhotonMaker')
photon = maker.PhotonMaker
photon.ConfidenceLevelBase = []
photon.ConfidenceLevelSwitch = []
photon.ConvertedPhotons = True
photon.UnconvertedPhotons = True
photon.PtCut = 250
photon.OutputLevel = ERROR
maker.InputPrimaryVertices = 'None'  ## NB: it saves a lot of CPU time

from Configurables import GaudiSequencer
kaliSeq = GaudiSequencer('KaliSeq')
kaliSeq.MeasureTime = True
kaliSeq.OutputLevel = ERROR
kaliSeq.Members = []
kaliSeq.Members += [proto]
kaliSeq.Members += [maker]

from Configurables import Pi0
kali = Pi0(
    "KaliPi0",
    ## specific cuts :
    Cuts={
        'PtGamma': 250 * MeV,
        'SpdCut': 0.1 * MeV
    },
    ##---toremove for testing
    #Cuts = { 'PtGamma' : 250 * MeV , 'SpdCut'  : 10000 * MeV  } ,
    ##---toremove for testing
    Pi0Cut=" PT > 200 * MeV * ( 7 - ETA ) ",  ## Cut for pi0
    ## general configuration
    NTupleLUN="KALIPI0",
    HistoPrint=True,
    NTuplePrint=True,
    Inputs=['Phys/%s/Particles' % maker.name()],
    Mirror=True,
    HistoProduce=True,  #False,
    NTupleProduce=True)
kaliSeq.Members += [kali]

algs = []
algs += [kaliSeq]


def action():
    """
    Reset DV-init sequence. IMPORTANT: It saves a lot of CPU time!!!
    """

    from Gaudi.Configuration import allConfigurables

    ## reser DaVinci sequences:
    for seq in ('DaVinciInitSeq', 'DaVinciEventInitSeq', 'DaVinviEventInitSeq',
                'MonitoringSeq', 'LumiSeq', 'IntegratorSeq'):
        if not seq in allConfigurables: continue
        iseq = getConfigurable(seq)
        if iseq and hasattr(iseq, 'Members'):
            iseq.Members = []


if '__main__' == __name__:
    """
    Messages in the online get redirected.
    Setup here the FMC message service

    @author M.Frank
    """
    Online = importOnline()

    app = Gaudi.ApplicationMgr()
    Configs.AuditorSvc().Auditors = []

    if 'LOGFIFO' in os.environ:
        app.MessageSvcType = 'LHCb::FmcMessageSvc'
        if Gaudi.allConfigurables.has_key('MessageSvc'):
            del Gaudi.allConfigurables['MessageSvc']
        msg = Configs.LHCb__FmcMessageSvc('MessageSvc')
        msg.fifoPath = os.environ['LOGFIFO']
        msg.LoggerOnly = True
        msg.doPrintAlways = False
        msg.OutputLevel = MSG_INFO  # Online.OutputLevel

    from Configurables import DaVinci
    DaVinci().DataType = '2018'
    DaVinci().InputType = 'DST'  # use SDSTs as an input
    DaVinci().PrintFreq = 10000
    DaVinci().IgnoreDQFlags = True
    DaVinci().EnableUnpack = []
    DaVinci().Lumi = False

    from Configurables import IODataManager
    IODataManager().DisablePFNWarning = True

    from Configurables import CondDB
    CondDB().IgnoreHeartBeat = True
    CondDB().EnableRunStampCheck = False
    #    CondDB().Simulation = False
    #    CondDB().UseDBSnapshot = True
    #    CondDB().DBSnapshotDirectory = "/group/online/hlt/conditions"
    #    CondDB().EnableRunChangeHandler = True
    #    CondDB().Tags["ONLINE"] = 'fake'
    #    CondDB().setProp("IgnoreHeartBeat", True)
    #    CondDB().Online = True

    Online = importOnline()
    rd = os.path.split(Online.ConditionsMapping)[0]
    sys.path.append(rd)

    Escher().CondDBtag = Online.CondDBTag
    Escher().DDDBtag = Online.DDDBTag

    if secondPass == 1:
        print "Second pass ---> Uses the new DB"
        CondDB().addLayer("/group/calo/CalibWork/Histos/LHCBCON-Calib")
    else:
        print "First pass ---> Uses the default DB"
        #---toremove for testing
        #CondDB().addLayer("/group/calo/CalibWork/Histos/LHCBCON-Calib")
        #---toremove for testing

    from Configurables import Gaudi__MultiFileCatalog
    Gaudi__MultiFileCatalog("FileCatalog").Catalogs = [
        'xmlcatalog_file:/tmp/kali_catalog.xml'
    ]

    DaVinci().UserAlgorithms = algs
    from Configurables import DataOnDemandSvc
    dod = DataOnDemandSvc(Dump=False)

    appendPostConfigAction(action)

    from Configurables import IODataManager
    IODataManager().DisablePFNWarning = True

    from Gaudi.Configuration import NTupleSvc, HistogramPersistencySvc
    output = NTupleSvc().Output
    NTupleSvc().Output += [
        "KALIPI0 DATAFILE='%s' TYPE='ROOT' OPT='NEW'" % tup_file
    ]

    ##
    #HistogramPersistencySvc ( OutputFile = '%s' % histo_file )#self.getProp('Histos') )
    ##

    from GaudiConf import IOHelper
    listDSTs = []
    for i in listFills:
        listDSTs.append(
            os.path.join(directory, pt.fileprefix()) + '_Fill_%d.dst' % (i))
    print listDSTs
    IOHelper('ROOT').inputFiles(listDSTs)

    from GaudiPython.Bindings import AppMgr
    from ROOT import gROOT
    gROOT.SetBatch()
    gaudi = AppMgr()

    gaudi.run(-1)

## THE END

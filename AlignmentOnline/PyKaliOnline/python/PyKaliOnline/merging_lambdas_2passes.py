#!/bin/python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

file1 = open('/group/calo/CalibWork/Histos/FirstPass/7/lambda.txt', 'r')
file2 = open('/group/calo/CalibWork/Histos/SecondPass/7/lambda.txt', 'r')

listLines1 = file1.readlines()
listLines1.sort()

listLines2 = file2.readlines()
listLines2.sort()

lambda1 = [None] * 12000
lambda2 = [None] * 12000
for line1 in listLines1:
    lambda1[int(line1.split('  ')[0])] = float(line1.split('  ')[1])
for line2 in listLines2:
    lambda2[int(line2.split('  ')[0])] = float(line2.split('  ')[1])

from time import ctime
now = ctime()

#lambdaFile = open('Calibration.xml','w')
lambdaFile = open('/group/calo/CalibWork/Histos/Calibration.xml', 'w')

lambdaFile.write(
    """<?xml version="1.0" encoding="ISO-8859-1"?> \n<!--- $Id: -->\n<!-- Jean-Francois Marchand -->\n<!--- Created : %s -->\n<!--- condDB for data -->\n<!DOCTYPE DDDB SYSTEM "../../../DTD/structure.dtd">\n<DDDB>\n <condition name = "Calibration">\n <param name = "size"       type = "int">   2 </param>\n    <paramVector name = "data" type = "double">\n      <!---   channel   dG  -->\n"""
    % now)

for i in range(0, 12000):
    if (lambda1[i] != None and lambda2[i] != None):
        lambdaFile.write("""%s %.12f\n""" % (i, lambda1[i] * lambda2[i]))
    if (lambda1[i] == None and lambda2[i] != None):
        lambdaFile.write("""%s %.12f\n""" % (i, lambda2[i]))
    if (lambda1[i] != None and lambda2[i] == None):
        lambdaFile.write("""%s %.12f\n""" % (i, lambda1[i]))
    if ((lambda1[i] == None and lambda2[i] != None)
            or (lambda1[i] != None and lambda2[i] == None)):
        print lambda1[i], "  ", lambda2[i]

lambdaFile.write("""    </paramVector>\n</condition>\n</DDDB>""")

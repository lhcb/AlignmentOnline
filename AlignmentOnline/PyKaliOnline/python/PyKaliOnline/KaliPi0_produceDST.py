#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# $Id: KaliPi0_produceDST.py$
# =============================================================================
## @file
#  The basic configuration for small DST production
# =============================================================================
## ============= the basic import ====================
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
import Configurables as Configs
import Gaudi.Configuration as Gaudi

import os, sys, socket, re
from Paths import Paths, importOnline

MSG_VERBOSE = 1
MSG_DEBUG = 2
MSG_INFO = 3
MSG_WARNING = 4
MSG_ERROR = 5
MSG_FATAL = 6
MSG_ALWAYS = 7

import argparse
parser = argparse.ArgumentParser(description="Input file delivery")
parser.add_argument("--index", type=int, dest="index", default=None)
parser.add_argument("--datadir", type=str, dest="datadir", default=None)
parser.add_argument("--workdir", type=str, dest="workdir", default=None)
args = parser.parse_args()

index = args.index
hn = socket.gethostname()
pt = Paths(index, hn, args.workdir, args.datadir)
directory = pt.dst_dir

#### Get fillNumber from occupancy task
fillNumber = 0
f = open('/group/calo/OccupancyAnalysis/xml/v0.xml', 'r')
for line in f.readlines():
    if 'fillNumber' in line:
        fillNumber = int(line.split("<!-- fillNumber: ")[1].split(" -->\n")[0])
f.close()
print fillNumber

dstname = os.path.join(directory, pt.fileprefix())


def insertSelection():
    from Configurables import GaudiSequencer, ProcessPhase
    recSeq = ProcessPhase("Reco")

    nuke = [
        "RecoRICHSeq", "RecoMUONSeq", "RecoPROTOSeq", "CaloPIDsSeq",
        "CaloPIDsCaloPIDsForCaloProcessor", "MoniGENERALSeq", "MoniTrSeq",
        "MoniPROTOSeq", "MoniHltSeq", "MoniCALOSeq", "MoniRICHSeq",
        "MoniVELOSeq", "MoniTrSeq", "MoniOTSeq", "MoniSTSeq", "MoniPROTOSeq",
        "MoniHltSeq"
    ]

    for m in nuke:
        seq = GaudiSequencer(m)
        seq.Members = []

    GaudiSequencer("RecoDecodingSeq").Members += ["OTTimeCreator"]


if '__main__' == __name__:
    """
    Messages in the online get redirected.
    Setup here the FMC message service

    @author M.Frank
    """
    Online = importOnline()

    app = Gaudi.ApplicationMgr()
    Configs.AuditorSvc().Auditors = []

    if 'LOGFIFO' in os.environ:
        app.MessageSvcType = 'LHCb::FmcMessageSvc'
        if Gaudi.allConfigurables.has_key('MessageSvc'):
            del Gaudi.allConfigurables['MessageSvc']
        msg = Configs.LHCb__FmcMessageSvc('MessageSvc')
        msg.fifoPath = os.environ['LOGFIFO']
        msg.LoggerOnly = True
        msg.doPrintAlways = False
        msg.OutputLevel = MSG_INFO  # Online.OutputLevel

    from Configurables import Brunel
    Brunel().Detectors = ['Spd', 'Prs', 'Ecal', 'Hcal']
    Brunel().DataType = '2016'
    Brunel().InputType = "MDF"
    Brunel().OutputType = 'DST'
    Brunel().EvtMax = -1
    Brunel().WriteFSR = False
    Brunel().Histograms = "None"
    Brunel().OnlineMode = True
    Brunel().PrintFreq = 1000
    Brunel().VetoHltErrorEvents = False
    #Brunel().OutputLevel = 4
    Brunel().PhysFilterMask = [0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF]
    Brunel().DatasetName = dstname

    from Configurables import IODataManager
    IODataManager().DisablePFNWarning = True

    from Configurables import L0Conf
    L0Conf().EnsureKnownTCK = False

    from Configurables import LHCb__PhotSelAlg
    import Gaudi.Configuration as GC

    GC.appendPostConfigAction(insertSelection)

    from Configurables import CondDB
    CondDB().Simulation = False
    CondDB().UseDBSnapshot = True
    CondDB().DBSnapshotDirectory = "/group/online/hlt/conditions"
    CondDB().EnableRunChangeHandler = True
    CondDB().Tags["ONLINE"] = 'fake'
    CondDB().setProp("IgnoreHeartBeat", True)
    CondDB().Online = True

    Online = importOnline()
    rd = os.path.split(Online.ConditionsMapping)[0]
    sys.path.append(rd)
    import AllHlt1

    CondDB().RunChangeHandlerConditions = AllHlt1.ConditionMap

    from Configurables import DaVinci
    Brunel().CondDBtag = Online.CondDBTag
    Brunel().DDDBtag = Online.DDDBTag

    from Configurables import Gaudi__MultiFileCatalog
    Gaudi__MultiFileCatalog("FileCatalog").Catalogs = [
        'xmlcatalog_file:/tmp/kali_catalog.xml'
    ]

    base_dir = pt.dst_dir
    runnrs = [int(r) for r in Online.DeferredRuns]
    from itertools import product
    files = []
    if runnrs:
        for run in sorted(runnrs):
            re_file = re.compile(r"(?:Run_)?(0*%d).*\.(mdf|raw)" % run)
            file_dir = base_dir
            if str(run) in os.listdir(base_dir):
                file_dir = os.path.join(base_dir, str(run))
            files += sorted([
                os.path.join(file_dir, f) for f in os.listdir(file_dir)
                if re_file.match(f)
            ])
    else:
        files += sorted(
            [os.path.join(base_dir, f) for f in os.listdir(base_dir)])

    from GaudiConf import IOHelper
    IOHelper('MDF').inputFiles(files)

    from GaudiPython.Bindings import AppMgr
    from ROOT import gROOT
    gROOT.SetBatch()
    gaudi = AppMgr()

    gaudi.run(-1)

## THE END

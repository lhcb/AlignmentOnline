#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# $Id: KaliPi0_autoProduceDST.py$
# =============================================================================

## ============= the basic import ====================
import os, sys, socket, re

from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
import Configurables as Configs
import Gaudi.Configuration as Gaudi

from Paths import Paths, importOnline

MSG_VERBOSE = 1
MSG_DEBUG = 2
MSG_INFO = 3
MSG_WARNING = 4
MSG_ERROR = 5
MSG_FATAL = 6
MSG_ALWAYS = 7

import argparse
parser = argparse.ArgumentParser(description="Input file delivery")
parser.add_argument("--index", type=int, dest="index", default=None)
parser.add_argument("--datadir", type=str, dest="datadir", default=None)
parser.add_argument("--workdir", type=str, dest="workdir", default=None)
args = parser.parse_args()

index = args.index
hn = socket.gethostname()
pt = Paths(index, hn, args.workdir, args.datadir)
directory = pt.dst_dir

#### Get fillNumber from occupancy task
fillNumber = 0
f = open('/group/calo/OccupancyAnalysis/xml/v0.xml', 'r')
for line in f.readlines():
    if 'fillNumber' in line:
        fillNumber = int(line.split("<!-- fillNumber: ")[1].split(" -->\n")[0])
f.close()

print fillNumber

dstname = os.path.join(directory, pt.fileprefix()) + '_Fill_%d' % (fillNumber)

#fillNumber = 206404 #### to be removed, was used for the first calibration of 2018


def insertSelection():
    from Configurables import GaudiSequencer, ProcessPhase
    recSeq = ProcessPhase("Reco")

    nuke = [
        "RecoRICHSeq", "RecoMUONSeq", "RecoPROTOSeq", "CaloPIDsSeq",
        "CaloPIDsCaloPIDsForCaloProcessor", "RecoRICHFUTURESeq",
        "MoniGENERALSeq", "MoniTrSeq", "MoniPROTOSeq", "MoniHltSeq",
        "MoniCALOSeq", "MoniRICHSeq", "MoniMUONSeq", "MoniVELOSeq",
        "MoniTrSeq", "MoniOTSeq", "MoniSTSeq", "MoniPROTOSeq", "MoniHltSeq"
    ]

    for m in nuke:
        seq = GaudiSequencer(m)
        seq.Members = []

    GaudiSequencer("RecoDecodingSeq").Members += ["OTTimeCreator"]


if '__main__' == __name__:
    """
    Messages in the online get redirected.
    Setup here the FMC message service

    @author M.Frank
    """
    Online = importOnline()

    app = Gaudi.ApplicationMgr()
    Configs.AuditorSvc().Auditors = []
    app.SvcOptMapping.append('LHCb::FmcMessageSvc/MessageSvc')

    if 'LOGFIFO' in os.environ:
        app.MessageSvcType = 'LHCb::FmcMessageSvc'
        if Gaudi.allConfigurables.has_key('MessageSvc'):
            del Gaudi.allConfigurables['MessageSvc']
        msg = Configs.LHCb__FmcMessageSvc('MessageSvc')
        msg.fifoPath = os.environ['LOGFIFO']
        msg.LoggerOnly = True
        msg.doPrintAlways = False
        msg.OutputLevel = MSG_INFO  # Online.OutputLevel

    from Configurables import Brunel
    Brunel().Detectors = ['Spd', 'Prs', 'Ecal', 'Hcal']
    Brunel().DataType = '2018'
    Brunel().InputType = "MDF"
    Brunel().OutputType = 'DST'
    Brunel().EvtMax = -1
    Brunel().WriteFSR = False
    Brunel().Histograms = "None"
    Brunel().OnlineMode = True
    Brunel().PrintFreq = 1000
    Brunel().VetoHltErrorEvents = False
    #Brunel().OutputLevel = 4
    Brunel().PhysFilterMask = [0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF]
    Brunel().DatasetName = dstname

    Brunel().OnlineMode = True
    Brunel().CondDBtag = Online.CondDBTag
    Brunel().DDDBtag = Online.DDDBTag
    Brunel().UseDBSnapshot = False  #True
    Brunel().PartitionName = os.environ.get('PARTITION_NAME', 'TEST')

    from Configurables import RecMoniConf, RecSysConf

    # Disable HLT2 monitoring
    RecMoniConf().MoniSequence = RecMoniConf().KnownMoniSubdets
    if 'Hlt' in RecMoniConf().MoniSequence:
        RecMoniConf().MoniSequence.remove('Hlt')

    # Hack to disable old RICH monitoring
    if "RICH" in RecMoniConf().MoniSequence:
        RecMoniConf().MoniSequence.remove('RICH')

    # if running old RICH, remove the new one ...
    if "RICHFUTURE" in RecMoniConf().MoniSequence:
        RecMoniConf().MoniSequence.remove('RICHFUTURE')

    from Configurables import CondDB
    CondDB().Online = True
    CondDB().EnableRunChangeHandler = True
    CondDB().UseDBSnapshot = False
    CondDB().EnableRunStampCheck = False
    CondDB().Tags['ONLINE'] = Online.OnlDBTag  #needed when using gitDB

    import sys
    try:
        import All
    except ImportError:
        rd = '/group/online/hlt/conditions/RunChangeHandler'
        sys.path.append(rd)
        import All
    CondDB().RunChangeHandlerConditions = All.ConditionMap

    from Configurables import IODataManager
    IODataManager().DisablePFNWarning = True

    from Configurables import L0Conf
    L0Conf().EnsureKnownTCK = False

    import Gaudi.Configuration as GC
    GC.appendPostConfigAction(insertSelection)

    rd = os.path.split(Online.ConditionsMapping)[0]
    sys.path.append(rd)

    from Configurables import Gaudi__MultiFileCatalog
    Gaudi__MultiFileCatalog("FileCatalog").Catalogs = [
        'xmlcatalog_file:/tmp/kali_catalog.xml'
    ]

    base_dir = pt.dst_dir
    runnrs = [int(r) for r in Online.DeferredRuns]
    print "----- Deferred runs -----"
    print Online.DeferredRuns

    from itertools import product
    files = []
    if runnrs:
        for run in sorted(runnrs):
            re_file = re.compile(r"(?:Run_)?(0*%d).*\.(mdf|raw)" % run)
            file_dir = base_dir
            if str(run) in os.listdir(base_dir):
                file_dir = os.path.join(base_dir, str(run))
            files += sorted([
                os.path.join(file_dir, f) for f in os.listdir(file_dir)
                if re_file.match(f)
            ])
    else:
        files += sorted(
            [os.path.join(base_dir, f) for f in os.listdir(base_dir)])

    print "-------- Files list --------"
    print files
    from GaudiConf import IOHelper
    IOHelper('MDF').inputFiles(files)

    from GaudiPython.Bindings import AppMgr
    from ROOT import gROOT
    gROOT.SetBatch()
    gaudi = AppMgr()

    gaudi.run(-1)

## THE END

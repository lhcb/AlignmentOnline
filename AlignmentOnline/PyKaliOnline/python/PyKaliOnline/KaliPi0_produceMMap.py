#!/bin/python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#--> convert the ROOT (TTree) to MMap file

from GaudiPython import gbl
pi0Calib = gbl.Calibration.Pi0Calibration

import os

## ============= the basic import ====================
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
import Configurables as Configs
import Gaudi.Configuration as Gaudi

import os, sys, socket, re
from Paths import Paths, importOnline

MSG_VERBOSE = 1
MSG_DEBUG = 2
MSG_INFO = 3
MSG_WARNING = 4
MSG_ERROR = 5
MSG_FATAL = 6
MSG_ALWAYS = 7

import argparse
parser = argparse.ArgumentParser(description="Input file delivery")
parser.add_argument("--index", type=int, dest="index", default=None)
parser.add_argument("--datadir", type=str, dest="datadir", default=None)
parser.add_argument("--workdir", type=str, dest="workdir", default=None)
parser.add_argument("--secondPass", type=int, dest="secondPass", default=None)
args = parser.parse_args()

index = args.index
hn = socket.gethostname()
pt = Paths(index, hn, args.workdir, args.datadir)
directory = pt.dst_dir
secondPass = args.secondPass

if secondPass == 1:
    tup_file = os.path.join(directory, pt.fileprefix()) + '_SecondPass.root'
    mmap_file = os.path.join(directory, pt.fileprefix()) + '_SecondPass.MMap'
else:
    tup_file = os.path.join(directory, pt.fileprefix()) + '_FirstPass.root'
    mmap_file = os.path.join(directory, pt.fileprefix()) + '_FirstPass.MMap'
tuplename = "KaliPi0/Pi0-Tuple"

if '__main__' == __name__:
    pi0Calib.Pi0CalibrationFile(tup_file, tuplename, mmap_file)

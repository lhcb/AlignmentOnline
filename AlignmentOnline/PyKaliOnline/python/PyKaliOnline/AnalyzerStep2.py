###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Communicator import *
import os, socket, re, importlib, time, random, glob
from RunKali import Ntupleprod, MMapprod, Histoprod, MergeHistograms, NtupleprodStep2
from PyKaliOnline.Paths import *


def run_online(work_directory, data_directory):
    fifo = os.environ.get('LOGFIFO', None)
    if not fifo:
        run(0, work_directory, data_directory)
    else:
        old_stdout = os.dup(sys.stdout.fileno())
        fifo = open(fifo, 'w')
        os.dup2(fifo.fileno(), sys.stdout.fileno())
        run(0, work_directory, data_directory)
        os.close(fifo.fileno())
        os.dup2(old_stdout, sys.stdout.fileno())


def run(index, work_directory, data_directory):
    # Start the communicator:
    com = Communicator("AligWrk_%d" % index)

    # FSM loop
    state = State.NOT_READY
    com.set_status(state)
    n_it = 0
    p_it = 1
    hn = socket.gethostname()
    pt = Paths(index, hn, work_directory, data_directory)
    while True:
        command = com.get_command()
        if command.startswith('configure') and state == State.NOT_READY:
            state = State.READY
        elif command.startswith('start') and state == State.READY:
            state = State.RUNNING
            com.set_status(state)
            n_it += 1

            filledmmap = pt.getmmapfiles()

            ## Clear everything that was
            ## left from the previous run
            ## Uncomment these lines if you feel
            ## that it is necessary
            print "Iteration: %d" % n_it

            if p_it < 3:  #should be 3 (2 main iteration)

                if n_it == 1:  # and p_it == 1:

                    if p_it == 1:
                        for f in glob.glob(
                                '/localdisk/Alignment/Calo/*_FirstPass.root'):
                            os.remove(f)
                        for f in glob.glob(
                                '/localdisk/Alignment/Calo/*_SecondPass.root'):
                            os.remove(f)
                        for f in glob.glob(
                                '/localdisk/Alignment/Calo/*_FirstPass.MMap'):
                            os.remove(f)
                        for f in glob.glob(
                                '/localdisk/Alignment/Calo/*_SecondPass.MMap'):
                            os.remove(f)

                    if p_it == 1:
                        # First pass: Produces ntuples with default DB (lambdas == 1)
                        print "first pass"
                        NtupleprodStep2(
                            index=index,
                            work_directory=work_directory,
                            data_directory=data_directory,
                            secondPass=0)
                    else:  #if p_it==2:
                        # Second pass: Produces ntuples with DB computed during the first pass
                        NtupleprodStep2(
                            index=index,
                            work_directory=work_directory,
                            data_directory=data_directory,
                            secondPass=1)

                    rootfiles = pt.getrootfiles()
                    for i in range(len(rootfiles)):
                        os.system('chmod a+rwx ' + rootfiles[i])

                    if p_it == 1:
                        print "MMAP prod"
                        MMapprod(
                            index=index,
                            work_directory=work_directory,
                            data_directory=data_directory,
                            secondPass=0)
                    else:
                        MMapprod(
                            index=index,
                            work_directory=work_directory,
                            data_directory=data_directory,
                            secondPass=1)

                    mmapfiles = pt.getmmapfiles()
                    for i in range(len(mmapfiles)):
                        os.system('chmod a+rwx ' + mmapfiles[i])

                    if p_it == 1:
                        Histoprod(
                            index=index,
                            work_directory=work_directory,
                            data_directory=data_directory,
                            iteration=n_it,
                            secondPass=0)
                    else:
                        Histoprod(
                            index=index,
                            work_directory=work_directory,
                            data_directory=data_directory,
                            iteration=n_it,
                            secondPass=1)

                    state = State.PAUSED

                if n_it < NIteration + 1:
                    if p_it == 1:
                        Histoprod(
                            index=index,
                            work_directory=work_directory,
                            data_directory=data_directory,
                            iteration=n_it,
                            secondPass=0)
                    else:
                        Histoprod(
                            index=index,
                            work_directory=work_directory,
                            data_directory=data_directory,
                            iteration=n_it,
                            secondPass=1)
                    if n_it == NIteration:  # and p_it==1:
                        n_it = 0
                        p_it += 1

                    state = State.PAUSED

            else:
                state = State.PAUSED

            state = State.PAUSED

        elif command.startswith('stop') and state == State.PAUSED:
            state = State.READY
        elif command.startswith('reset'):
            state = State.NOT_READY
            break
        else:
            print 'analyzer: bad transition from %s to %s' % (state, command)
            state = State.ERROR
            break

        time.sleep(random.uniform(0.5, 1.5))
        # Set the status
        com.set_status(state)

    time.sleep(random.uniform(0.5, 1.5))
    # Set the status one last time.
    com.set_status(state)


if __name__ == '__main__':
    run(0)

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import *

import argparse
parser = argparse.ArgumentParser(description="Input file delivery")
parser.add_argument(
    "--input-files", type=str, dest="input_files", nargs='*', default=None)
parser.add_argument(
    "--output-file", type=str, dest="output_file", default=None)
args = parser.parse_args()

input_files = args.input_files
output_file = args.output_file

hists = TH2D("hists", "hists", 11383, 0, 11383, 100, 0.0, 250.0)
#hists_bg = TH2D("hists_bg", "hists_bg", 11383, 0, 11383, 100, 0.0, 250.0);

#i = 0
for file in input_files:
    #print i," ",file
    tfile = TFile.Open(file)
    if tfile:
        #i += 1
        h = tfile.Get("hists")
        #h_bg = tfile.Get("hists_bg")
        hists.Add(h)
        #hists_bg.Add(h_bg)
    tfile.Close()

f = TFile(output_file, "RECREATE")
hists.Write()
#hists_bg.Write()
f.Write()
f.Close()

#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# $Id: KaliPi0_produceNtuple.py$
# =============================================================================
## ============= the basic import ====================
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
import Configurables as Configs
import Gaudi.Configuration as Gaudi

import os, sys, socket, re
from Paths import Paths, importOnline

MSG_VERBOSE = 1
MSG_DEBUG = 2
MSG_INFO = 3
MSG_WARNING = 4
MSG_ERROR = 5
MSG_FATAL = 6
MSG_ALWAYS = 7

import argparse
parser = argparse.ArgumentParser(description="Input file delivery")
parser.add_argument("--index", type=int, dest="index", default=None)
parser.add_argument("--datadir", type=str, dest="datadir", default=None)
parser.add_argument("--workdir", type=str, dest="workdir", default=None)
parser.add_argument("--secondPass", type=int, dest="secondPass", default=None)
args = parser.parse_args()

index = args.index
hn = socket.gethostname()
pt = Paths(index, hn, args.workdir, args.datadir)
directory = pt.dst_dir
secondPass = args.secondPass

print "secondPass= ", secondPass

if secondPass == 1:
    tup_file = os.path.join(directory, pt.fileprefix()) + '_SecondPass.root'
    dstname = os.path.join(directory, pt.fileprefix())
else:
    tup_file = os.path.join(directory, pt.fileprefix()) + '_FirstPass.root'
    dstname = os.path.join(directory, pt.fileprefix())

from Configurables import OffLineCaloRecoConf
from Configurables import OffLineCaloPIDsConf

OffLineCaloRecoConf(
    EnableRecoOnDemand=True,
    UseTracks=True,  #False ,         ## do not use tracks!
    UseSpd=True,  #False ,
    #UsePrs             = False ,
    ForceDigits=False,
    MeasureTime=True,
    OutputLevel=ERROR)

OffLineCaloPIDsConf(
    EnablePIDsOnDemand=True, MeasureTime=True, OutputLevel=ERROR)

from GlobalReco.GlobalRecoConf import NeutralProtoPAlg
proto = NeutralProtoPAlg(
    "NeutralProtoPMaker",
    LightMode=True,  ## "ligght-mode", no PIDs
    HyposLocations=['Rec/Calo/Photons'],  ## only single photons
    OutputLevel=ERROR)

from ParticleMaker.ParticleMakerConf import PhotonMakerAlg, PhotonMaker
maker = PhotonMakerAlg(
    'StdLooseAllPhotons', OutputLevel=ERROR, DecayDescriptor='Gamma')
maker.addTool(PhotonMaker, name='PhotonMaker')
photon = maker.PhotonMaker
photon.ConfidenceLevelBase = []
photon.ConfidenceLevelSwitch = []
photon.ConvertedPhotons = True
photon.UnconvertedPhotons = True
photon.PtCut = 250
photon.OutputLevel = ERROR
maker.InputPrimaryVertices = 'None'  ## NB: it saves a lot of CPU time

from Configurables import GaudiSequencer, DaVinciInit
kaliSeq = GaudiSequencer('KaliSeq')
kaliSeq.MeasureTime = True
kaliSeq.OutputLevel = ERROR
kaliSeq.Members = []
kaliSeq.Members += [proto]
kaliSeq.Members += [maker]

from Configurables import Pi0
kali = Pi0(
    "KaliPi0",
    ## specific cuts :
    Cuts={
        'PtGamma': 250 * MeV,
        'SpdCut': 0.1 * MeV
    },
    ## cut for pi0 :
    #Pi0Cut         = "PT > 550 * MeV", #self.getProp ( 'Pi0Cut' )      ,
    Pi0Cut=" PT > 200 * MeV * ( 7 - ETA ) ",  ## Cut for pi0
    ## general configuration
    NTupleLUN="KALIPI0",
    HistoPrint=True,
    NTuplePrint=True,
    Inputs=['Phys/%s/Particles' % maker.name()],
    Mirror=True,
    HistoProduce=False,
    NTupleProduce=True)
kaliSeq.Members += [kali]

algs = []
algs += [kaliSeq]


def action():
    """
    Reset DV-init sequence. IMPORTANT: It saves a lot of CPU time!!!
    """

    from Gaudi.Configuration import allConfigurables

    ## reser DaVinci sequences:
    for seq in ('DaVinciInitSeq', 'DaVinciEventInitSeq', 'DaVinviEventInitSeq',
                'MonitoringSeq', 'LumiSeq', 'IntegratorSeq'):
        if not seq in allConfigurables: continue
        iseq = getConfigurable(seq)
        if iseq and hasattr(iseq, 'Members'):
            iseq.Members = []


if '__main__' == __name__:
    """
    Messages in the online get redirected.
    Setup here the FMC message service

    @author M.Frank
    """
    Online = importOnline()

    app = Gaudi.ApplicationMgr()
    Configs.AuditorSvc().Auditors = []

    if 'LOGFIFO' in os.environ:
        app.MessageSvcType = 'LHCb::FmcMessageSvc'
        if Gaudi.allConfigurables.has_key('MessageSvc'):
            del Gaudi.allConfigurables['MessageSvc']
        msg = Configs.LHCb__FmcMessageSvc('MessageSvc')
        msg.fifoPath = os.environ['LOGFIFO']
        msg.LoggerOnly = True
        msg.doPrintAlways = False
        msg.OutputLevel = MSG_INFO  # Online.OutputLevel

    from Configurables import DaVinci
    DaVinci().DataType = '2018'
    DaVinci().InputType = 'DST'  # use SDSTs as an input
    DaVinci().PrintFreq = 10000
    DaVinci().IgnoreDQFlags = True
    DaVinci().EnableUnpack = []
    DaVinci().Lumi = False

    from Configurables import IODataManager
    IODataManager().DisablePFNWarning = True

    from Configurables import CondDB
    CondDB().Simulation = False
    CondDB().UseDBSnapshot = True
    CondDB().DBSnapshotDirectory = "/group/online/hlt/conditions"
    CondDB().EnableRunChangeHandler = True
    CondDB().Tags["ONLINE"] = 'fake'
    CondDB().setProp("IgnoreHeartBeat", True)
    CondDB().Online = True

    Online = importOnline()
    rd = os.path.split(Online.ConditionsMapping)[0]
    sys.path.append(rd)
    import AllHlt1

    CondDB().RunChangeHandlerConditions = AllHlt1.ConditionMap

    DaVinci().CondDBtag = Online.CondDBTag
    DaVinci().DDDBtag = Online.DDDBTag

    if secondPass == 1:
        print "Second pass ---> Uses the new DB"
        CondDB().addLayer(
            dbFile="/group/calo/CalibWork/Histos/LHCBCOND-Calib.db",
            dbName="LHCBCOND")
    else:
        print "First pass ---> Uses the default DB"

    from Configurables import Gaudi__MultiFileCatalog
    Gaudi__MultiFileCatalog("FileCatalog").Catalogs = [
        'xmlcatalog_file:/tmp/kali_catalog.xml'
    ]

    DaVinci().UserAlgorithms = algs
    from Configurables import DataOnDemandSvc
    dod = DataOnDemandSvc(Dump=False)

    appendPostConfigAction(action)

    from Configurables import IODataManager
    IODataManager().DisablePFNWarning = True

    from Gaudi.Configuration import NTupleSvc, HistogramPersistencySvc
    output = NTupleSvc().Output
    NTupleSvc().Output += [
        "KALIPI0 DATAFILE='%s' TYPE='ROOT' OPT='NEW'" % tup_file
    ]

    from GaudiConf import IOHelper
    IOHelper('ROOT').inputFiles(["%s.dst" % dstname])

    from GaudiPython.Bindings import AppMgr
    from ROOT import gROOT
    gROOT.SetBatch()
    gaudi = AppMgr()

    gaudi.run(-1)

## THE END

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from time import sleep, asctime
import os
# to use the git CondDB
#os.environ['GITCONDDBPATH'] = '/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb.test'

from PyKaliOnline.Paths import *
from Communicator import *


def run(directory='.'):
    # Start the communicator:
    com = Communicator('AligDrv_0')
    # FSM loop
    state = State.NOT_READY
    com.set_status(state)
    n_it = 0
    i_it = 1
    p_it = 1
    pt = Paths()
    gz_dir = pt.gz_dir
    dst_dir = pt.dst_dir
    store_location = pt.store_location()

    while True:
        command = com.get_command()
        if command == 'configure' and state == State.NOT_READY:
            ## Remove, if something has left from the previous run
            dbases = [
                os.path.join(gz_dir, f) for f in os.listdir(gz_dir)
                if re.match(r'.*\.(db)$', f) or re.match(r'.*\.(gz)$', f)
            ]
            for db in dbases:
                os.remove(db)
            stored = [
                os.path.join(pt.store_location(), f)
                for f in os.listdir(pt.store_location())
            ]
            for st in stored:
                os.remove(st)
            if os.path.exists(pt.files_to_merge()):
                os.remove(pt.files_to_merge())
            if os.path.exists(pt.files_to_merge() + 'c'):
                os.remove(pt.files_to_merge() + 'c')
            state = State.READY

        elif command == 'start' and state == State.READY:
            state = State.RUNNING

        elif command == 'pause' and state == State.RUNNING:
            state = State.PAUSED
            com.set_status(state)

            if n_it < NIteration and i_it < 3:  #MaxIt:
                n_it += 1
                print asctime(
                ), "Hi, I'm the iterator and I'm running", p_it, i_it, n_it
                state = State.READY

            else:
                print 'iterator done'
                state = State.READY

        elif command == 'stop' and state in (State.RUNNING, State.READY):
            state = State.READY
        elif command == 'reset':
            state = State.NOT_READY
            break
        else:
            print 'iterator: bad transition from %s to %s' % (state, command)
            state = State.ERROR
            break
        # Set the status
        com.set_status(state)

    # Set our status one last time
    com.set_status(state)


if __name__ == '__main__':
    run()

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import PyKaliOnline
import subprocess, re, socket
import shlex
import glob

partition = os.environ.get("PARTITIONNAME", "LHCbA")
lines = []
if partition == "TEST":
    dv_path = "/home/dsavrina/cmtuser/DaVinciDev_v40r2"
    if os.path.exists("env_cache"):
        with open("env_cache") as cache:
            for line in cache:
                lines.append(line.strip())
    else:
        p = subprocess.Popen([os.path.join(dv_path, 'run'), '--sh'],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        o, e = p.communicate()
        with open("env_cache", 'w') as cache:
            cache.write(o)
            lines = [line.strip() for line in o.split('\n')]
else:
    dv_path = "/group/calo/cmtuser/DaVinciDev_v42r3_cleanup"
    ## When running on the farm, use the pre-existing environment cache
    env_file = os.path.join(dv_path, 'setup.x86_64-slc6-gcc49-opt.vars')
    p = subprocess.Popen(
        "source %s; printenv" % env_file,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    o, e = p.communicate()
    lines = []
    for line in o.split('\n'):
        s = line.strip().split('=', 1)
        if not s or len(s) != 2:
            continue
        lines.append('export {0}="{1}";'.format(*s))

# Parse the output into a dictionary.
# Each contains one variable and looks like: "export FOO_bar='baz'"
exclude = set(['module'])
re_env = re.compile("""^export ([\w_]+)=["'](.*)["'];?$""")
dv_env = os.environ.copy()
for line in lines:
    if not line:
        continue
    result = re_env.match(line)
    if not result:
        print "couldn't match", line
    elif result.group(1) in exclude:
        continue
    else:
        dv_env[result.group(1)] = result.group(2)

homedir = os.environ['PYKALIONLINEROOT'] + '/python/PyKaliOnline/'

dv_env['RUNINFO'] = "/group/online/dataflow/options/LHCbA/HLT"
dv_env['PYTHONPATH'] = "/group/online/dataflow/options/LHCbA/HLT:" + dv_env[
    'PYTHONPATH']
dv_env['PYTHONPATH'] += (':%s' % homedir.rsplit('/', 1)[0])


def autoDSTprod(index=0,
                work_directory="/group/calo/CalibWork/",
                data_directory='/localdisk/Alignment/Calo'):
    command = 'python ' + os.path.join(
        homedir,
        'KaliPi0_autoProduceDST.py --index=%i --workdir="%s" --datadir="%s"' %
        (index, work_directory, data_directory))
    p = subprocess.call(shlex.split(command), env=dv_env)


def DSTprod(index=0,
            work_directory="/group/calo/CalibWork/",
            data_directory='/localdisk/Alignment/Calo'):
    command = 'python ' + os.path.join(
        homedir,
        'KaliPi0_produceDST.py --index=%i --workdir="%s" --datadir="%s"' %
        (index, work_directory, data_directory))
    p = subprocess.call(shlex.split(command), env=dv_env)


def Ntupleprod(index=0,
               work_directory="/group/calo/CalibWork/",
               data_directory='/localdisk/Alignment/Calo',
               secondPass=0):
    command = 'python ' + os.path.join(
        homedir,
        'KaliPi0_produceNtuple.py --index=%i --workdir="%s" --datadir="%s" --secondPass=%d'
        % (index, work_directory, data_directory, secondPass))
    p = subprocess.call(shlex.split(command), env=dv_env)


def NtupleprodStep2(index=0,
                    work_directory="/group/calo/CalibWork/",
                    data_directory='/localdisk/Alignment/Calo',
                    secondPass=0):
    # torem
    # was used for 2rd step... # cmd = 'rm -rf /group/calo/CalibWork/Histos/LHCBCON-Calib/Conditions/Ecal/Calibration/Calibration.xml'
    # was used for 2rd step... # p4 = subprocess.call(shlex.split(cmd), env = dv_env)
    # was used for 2rd step... # cmd = 'cp /group/calo/CalibWork/plotsCalib/2018/April/Calibration.xml /group/calo/CalibWork/Histos/LHCBCON-Calib/Conditions/Ecal/Calibration/Calibration.xml'
    # was used for 2rd step... # p5 = subprocess.call(shlex.split(cmd), env = dv_env)

    command = 'python ' + os.path.join(
        homedir,
        'KaliPi0_produceNtupleStep2.py --index=%i --workdir="%s" --datadir="%s" --secondPass=%d'
        % (index, work_directory, data_directory, secondPass))
    p = subprocess.call(shlex.split(command), env=dv_env)


def MMapprod(index=0,
             work_directory="/group/calo/CalibWork/",
             data_directory='/localdisk/Alignment/Calo',
             secondPass=0):
    command = 'python ' + os.path.join(
        homedir,
        'KaliPi0_produceMMap.py --index=%i --workdir="%s" --datadir="%s" --secondPass=%d'
        % (index, work_directory, data_directory, secondPass))
    p = subprocess.call(shlex.split(command), env=dv_env)


def Histoprod(index=0,
              work_directory="/group/calo/CalibWork/",
              data_directory='/localdisk/Alignment/Calo',
              iteration=0,
              secondPass=False):
    command = 'python ' + os.path.join(
        homedir,
        'KaliPi0_produceHisto.py --index=%i --workdir="%s" --datadir="%s" --iteration %d --secondPass=%d'
        % (index, work_directory, data_directory, int(iteration), secondPass))
    p = subprocess.call(shlex.split(command), env=dv_env)


def RemoveHistograms(iteration=0):
    print " ----------- RemoveHistos -------------"
    if iteration == 1:
        for i in range(1, 9):
            print "Remove Merged Histo files"
            cmd1 = 'rm -rf /group/calo/CalibWork/Histos/Histos_%d.root' % (
                int(i))
            os.system(cmd1)


def MergeHistograms(iteration=0):
    print " ----------- MergeHistograms -------------"
    listFiles = glob.glob(
        "/group/calo/CalibWork/Histos/Kali*Fit%d*root" % (iteration))
    command = 'python ' + os.path.join(
        homedir,
        'merge_rootfiles.py --output-file /group/calo/CalibWork/Histos/Histos_%d.root --input-files '
        % (iteration))
    for f in listFiles:
        command += ' %s' % (f)
    p = subprocess.call(shlex.split(command), env=dv_env)


def MergeSubHistograms(iteration=0):

    command = 'hadd -f /group/calo/CalibWork/Histos/Histos_%d.root /group/calo/CalibWork/Histos/Histos_a_%d.root /group/calo/CalibWork/Histos/Histos_b_%d.root /group/calo/CalibWork/Histos/Histos_c_%d.root /group/calo/CalibWork/Histos/Histos_d_%d.root /group/calo/CalibWork/Histos/Histos_e_%d.root /group/calo/CalibWork/Histos/Histos_f_%d.root' % (
        iteration, iteration, iteration, iteration, iteration, iteration,
        iteration)
    p1 = subprocess.Popen(
        shlex.split(command), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (out1, err1) = p1.communicate()
    p1.wait()


def RemoveKaliHistograms():

    print "Remove all Histos files"
    listKaliFiles = glob.glob("/group/calo/CalibWork/Histos/Kali*.root")
    for f in listKaliFiles:
        #sh.rm('-rf',f)
        command1 = 'rm -rf %s' % (f)
        p3 = subprocess.call(shlex.split(command1), env=dv_env)


def DoCalibration(iteration=0):
    print " ----------- DoCalibration -------------"

    if iteration == 1:
        #-- Remove directories produced by the fit
        print "Remove directories used byt fit"
        for i in range(1, 9):
            cmd = 'rm -rf /group/calo/CalibWork/Histos/%d/' % (i)
            p4 = subprocess.call(shlex.split(cmd), env=dv_env)

    outputdir = "/group/calo/CalibWork/Histos"
    datatype = 'TH2D'
    tuplefile = "/group/calo/CalibWork/Histos/Histos_%d.root" % (
        int(iteration))
    command = 'python ' + os.path.join(
        homedir, 'do_calibration.py --filename %s --filetype %s ' %
        (tuplefile, datatype))
    command += "--outdir %s " % outputdir
    command += "--indir %s  " % outputdir
    command += "--year 2018 "
    command += "--iteration %s " % iteration
    p5 = subprocess.call(shlex.split(command), env=dv_env)


def PrepareForFirstPass():
    print "----------- Begin first pass --------"

    #-- Remove directories produced by the fit
    print "Remove directories used byt fit"
    for i in range(1, 9):
        cmd = 'rm -rf /group/calo/CalibWork/Histos/FirstPass/%d/' % (i)
        p4 = subprocess.call(shlex.split(cmd), env=dv_env)
        cmd = 'rm -rf /group/calo/CalibWork/Histos/SecondPass/%d/' % (i)
        p4 = subprocess.call(shlex.split(cmd), env=dv_env)
        cmd = 'rm -rf /group/calo/CalibWork/Histos/LHCBCOND-Calib.db'
        p4 = subprocess.call(shlex.split(cmd), env=dv_env)
        cmd = 'rm -rf /group/calo/CalibWork/Histos/LHCBCON-Calib/Conditions/Ecal/Calibration/Calibration.xml'
        p4 = subprocess.call(shlex.split(cmd), env=dv_env)


def PrepareForSecondPass(maxiter=0):
    print "------------ Produce DB for the second iteration ------------"

    # Move fits to an other directory
    for i in range(1, 8):
        cmd = 'cp -r /group/calo/CalibWork/Histos/%d /group/calo/CalibWork/Histos/FirstPass/' % i
        p = subprocess.call(shlex.split(cmd), env=dv_env)

    print "Copy xml file"
    # Copy DB xml file where it should be
    cmd = 'cp /group/calo/CalibWork/Histos/%d/Calibration.xml /group/calo/CalibWork/Histos/LHCBCON-Calib/Conditions/Ecal/Calibration/' % (
        int(maxiter))
    p = subprocess.call(shlex.split(cmd), env=dv_env)

    print "Create DB"
    # Create the new DB
    # command = 'python /cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v41r0/Tools/CondDBUI/scripts/copy_files_to_db.py -c sqlite_file:/group/calo/CalibWork/Histos/LHCBCOND-Calib.db/LHCBCOND -s /group/calo/CalibWork/Histos/LHCBCON-Calib'
    # p = subprocess.call(shlex.split(command), env = dv_env)

    #p1 = subprocess.call(shlex.split('cd /group/calo/CalibWork/Histos/LHCBCON-Calib'), env = dv_env)
    #p2 = subprocess.call(shlex.split('git init -q'), env = dv_env)
    #p3 = subprocess.call(shlex.split('cd -'), env = dv_env)
    p = subprocess.call(
        'cd /group/calo/CalibWork/Histos/LHCBCON-Calib; pwd; git init; cd -; pwd;',
        shell=True)
    #p2 = subprocess.call('git init', shell=True)
    #p3 = subprocess.call('cd -', shell=True)


def PrepareForThirdPass():

    print "----------- End of second pass --------"

    # Move fits to an other directory
    for i in range(0, 8):
        cmd = 'cp -r /group/calo/CalibWork/Histos/%d /group/calo/CalibWork/Histos/SecondPass/' % i
        p = subprocess.call(shlex.split(cmd), env=dv_env)

    cmd = 'python ' + os.path.join(homedir, 'merging_lambdas_2passes.py')
    p = subprocess.call(shlex.split(cmd), env=dv_env)


def fmDSTprod(index=0,
              work_directory="/group/calo/CalibWork/",
              data_directory='/localdisk/Alignment/Calo'):
    # Use our parsed environment to run a DaVinci command. Output goes to terminal.
    command = 'python ' + os.path.join(
        homedir,
        'KaliPi0_runOnfmDST.py --index=%i --workdir="%s" --datadir="%s"' %
        (index, work_directory, data_directory))
    p = subprocess.call(shlex.split(command), env=dv_env)


def MergeMMapToHisto(iteration, input_file):
    command = 'python ' + os.path.join(
        homedir, 'do_mmap2histo.py --iteration %d --input-file' % iteration)

    for fl in input_file:
        command = command + ' %s' % fl

    p = subprocess.call(shlex.split(command), env=dv_env)

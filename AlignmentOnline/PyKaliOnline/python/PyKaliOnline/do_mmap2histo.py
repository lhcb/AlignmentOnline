###############################################################################
# (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
year = '2018'

import sys
niter = int(sys.argv[-1])
print niter

#import argparse
#parser = argparse.ArgumentParser(description = "Input file delivery")
#parser.add_argument("--input-file", type = str, dest = "input_file", nargs = '*', default = None) ## list of input files
#parser.add_argument("--iteration" , type = int, dest = "niteration", default = 0)
#args = parser.parse_args()
#
#niter = args.niteration
#fileList = args.input_file

print fileList

from Gaudi.Configuration import *
from Configurables import LHCbApp
LHCbApp().DataType = year
LHCbApp().EvtMax = -1

from Configurables import CondDBAccessSvc, CondDB, UpdateManagerSvc
CondDB().UseOracle = False
CondDB().UseLatestTags = [year]

from GaudiPython import *
ApplicationMgr(OutputLevel=INFO, AppName="Pi0MMap2Histo")

from Configurables import Pi0MMap2Histo
pi02Histo = Pi0MMap2Histo("Pi0MMap2Histo")
pi02Histo.nworker = 1

pi02Histo.filenames = fileList

pi02Histo.outputDir = '/group/calo/CalibWork/Fit/'
pi02Histo.outputName = 'FirstPassFit%d' % niter
if (niter > 1):
    pi02Histo.lambdaFileName = "/group/calo/CalibWork/Fit/%d/lambda.txt" % (
        niter - 1)
#pi02Histo.OutputLevel = ERROR

from Configurables import GaudiSequencer
mainSeq = GaudiSequencer("MainSeq")
mainSeq.Members = [pi02Histo]
ApplicationMgr().TopAlg.append(mainSeq)
AppMgr().run(1)

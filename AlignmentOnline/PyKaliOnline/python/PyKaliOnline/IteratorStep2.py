###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from time import sleep, asctime
import os
from RunKali import RemoveHistograms, MergeHistograms, RemoveKaliHistograms, DoCalibration, PrepareForFirstPass, PrepareForSecondPass, PrepareForThirdPass
from PyKaliOnline.Paths import *
from Communicator import *


def run(directory='.'):
    # Start the communicator:
    com = Communicator('AligDrv_0')
    # FSM loop
    state = State.NOT_READY
    com.set_status(state)
    n_it = 0
    i_it = 1
    p_it = 1
    pt = Paths()
    gz_dir = pt.gz_dir
    dst_dir = pt.dst_dir
    store_location = pt.store_location()

    while True:
        command = com.get_command()
        if command == 'configure' and state == State.NOT_READY:
            ## Remove, if something has left from the previous run
            #    #print 'Exterminate!'
            dbases = [
                os.path.join(gz_dir, f) for f in os.listdir(gz_dir)
                if re.match(r'.*\.(db)$', f) or re.match(r'.*\.(gz)$', f)
            ]
            #print 'Exterminate!!!',dbases
            for db in dbases:
                os.remove(db)
            stored = [
                os.path.join(pt.store_location(), f)
                for f in os.listdir(pt.store_location())
            ]
            #print 'EXTERMINATE!!!',stored
            for st in stored:
                os.remove(st)
            if os.path.exists(pt.files_to_merge()):
                os.remove(pt.files_to_merge())
            if os.path.exists(pt.files_to_merge() + 'c'):
                os.remove(pt.files_to_merge() + 'c')

            state = State.READY

        elif command == 'start' and state == State.READY:
            state = State.RUNNING

        elif command == 'pause' and state == State.RUNNING:
            state = State.PAUSED
            com.set_status(state)

            if n_it < NIteration and i_it < 3:  #MaxIt:
                n_it += 1

                print asctime(
                ), "Hi, I'm the iterator and I'm running", p_it, i_it, n_it

                if n_it == 1 and i_it == 1:
                    PrepareForFirstPass(
                    )  ## Remove FirstPass/ and SecondPass directories

                RemoveHistograms(
                    n_it
                )  ## Remove Histo_*.root files (only at first iteration)
                MergeHistograms(n_it)  ## Merge Kali_*.root files
                DoCalibration(n_it)  ## Do fits and roduce new lambdas

                if n_it == NIteration:  # and i_it==1:
                    RemoveKaliHistograms()  ## Remove Kali_*.root files
                    if i_it == 1:
                        PrepareForSecondPass(
                            NIteration
                        )  ## Copy 1,2,3,4,5,6,7 directories to FirstPass; Copy xml file; Create DB
                    if i_it == 2:
                        PrepareForThirdPass(
                        )  ## Copy 1,2,3,4,5,6,7 directories to SecondPass
                    n_it = 0
                    i_it += 1

                state = State.RUNNING

            else:
                print 'iterator done'
                state = State.READY

        elif command == 'stop' and state in (State.RUNNING, State.READY):
            state = State.READY
        elif command == 'reset':
            state = State.NOT_READY
            break
        else:
            print 'iterator: bad transition from %s to %s' % (state, command)
            state = State.ERROR
            break
        # Set the status
        com.set_status(state)

    # Set our status one last time
    com.set_status(state)


if __name__ == '__main__':
    run()

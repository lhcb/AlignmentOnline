###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

gaudi_add_library(AlignOnlineLib
    SOURCES
        src/AlignOnlineXMLCopier.cpp
        src/ASDCollector.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            Alignment::AlignKernel
)

gaudi_add_module(AlignOnline
    SOURCES
        src/DBXferAlg.cpp
        src/DumpConditions.cpp
        src/HistogramResetter.cpp
    LINK
        AlignOnlineLib
        Gaudi::GaudiAlgLib
        Alignment::AlignmentInterfacesLib
        Alignment::AlignmentMonitoringLib
        Alignment::TAlignmentLib
        Online::GauchoLib
        Online::OnlineBase
        Online::OnlineAlignLib
)

gaudi_install(PYTHON)

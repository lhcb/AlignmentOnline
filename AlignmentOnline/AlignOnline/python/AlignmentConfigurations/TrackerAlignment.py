###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def configureAlignment():
    from Configurables import Escher, TAlignment
    Escher(
    ).HltFilterCode = "HLT_PASS( 'Hlt1CalibTrackingKPiDetachedDecision' )"
    Escher().PrintFreq = 1
    from TAlignment.ParticleSelections import defaultHLTD0Selection
    TAlignment().ParticleSelections = [defaultHLTD0Selection()]
    from TAlignment.TrackSelections import NoPIDTracksFromHlt
    TAlignment().TrackSelections = [NoPIDTracksFromHlt()]

    from TAlignment.AlignmentScenarios import configureTrackerAlignment, configureTrackerAlignmentITInternal, configureFinerTrackerAlignment
    configureTrackerAlignment()
    #configureFinerTrackerAlignment()
    #configureTrackerAlignmentITInternal() # Run this after vertical alignment

    # Adds detailed plots for ST
    from TrackMonitors.STPerformance import STPerformanceStudy
    STPerformanceStudy()

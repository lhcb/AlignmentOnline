###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def configureAlignment():
    from Configurables import TAlignment
    from TAlignment.TrackSelections import TEDVeloTracks
    from TAlignment.VertexSelections import configuredPVSelection

    TAlignment().TrackSelections = [TEDVeloTracks()]

    # add the default PV selection
    TAlignment().PVSelection = configuredPVSelection()

    # Configure the alignment
    from TAlignment.AlignmentScenarios import configureTEDVeloSensorAlignment
    configureTEDVeloSensorAlignment()

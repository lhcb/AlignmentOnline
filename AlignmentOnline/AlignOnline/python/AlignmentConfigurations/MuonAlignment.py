###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def configureAlignment():

    from Configurables import Escher, TAlignment

    Escher().HltFilterCode = "HLT_PASS( 'Hlt1DiMuonHighMassDecision' )"
    from TAlignment.TrackSelections import BestMuonTracks, BestMuonTracksPb
    TAlignment().TrackSelections = [BestMuonTracks('OnlineMuonAlignment')]
    # TAlignment().TrackSelections = [ BestMuonTracksPb('OnlineMuonAlignment') ] # TO be used during PbPb or pPb fills

    from TAlignment.AlignmentScenarios import configureMuonAlignment
    configureMuonAlignment()

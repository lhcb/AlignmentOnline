###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys
import add_files_to_gitconddb
args = sys.argv[:]

from Gaudi.Configuration import *
from Configurables import LHCbAlgsTests__TestTimeDecoderOdin as TimeDecoder
from Configurables import (RunChangeHandlerSvc, DBXferAlg, XmlCnvSvc, DDDBConf,
                           CondDB, EventClockSvc, FakeEventTime)
import RunOption
import HLT2Params
import sys
import os


def MakeTick(root, runnr):
    pth = root + "/onl/Conditions/Online/LHCb"
    if not os.access(pth, os.F_OK):
        try:
            os.makedirs(pth, 0777)
        except:
            print "Cannot create directory " + pth
    else:
        print "Path " + pth + " already exists"
    xmlf = open(pth + "/RunStamp.xml", "w")
    xmlf.write('<?xml version="1.0" encoding="ISO-8859-1"?>\n')
    xmlf.write('<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd">\n')
    xmlf.write('<DDDB>\n')
    xmlf.write(
        '<condition name="RunStamp"> <param name="RunNumber" type="int">' +
        str(runnr) + '</param> </condition>\n')
    xmlf.write('</DDDB>\n')
    xmlf.close()


excludeFiles = []
excludeFiles.append('online.xml')
try:
    import CondMap
except:
    print "Cannot Import CondMap. Trying All"
    try:
        import All as CondMap
    except:
        print "Cannot Import All either. Exiting with status code 2"
        sys.exit(2)
ks = CondMap.ConditionMap.keys()
DETS = ['velo', 'tt', 'it', 'ot', 'rich1', 'rich2', 'calo', 'muon']
detcomps = dict()
for d in DETS:
    detcomps[d] = []
dets = []
for i in range(len(ks)):
    k = ks[i]
    excl = False
    for j in range(len(excludeFiles)):
        if k.find(excludeFiles[j]) >= 0:
            excl = True
            break
    if excl:
        continue
    detxml = k[k.rfind('/') + 1:]
    detxml = detxml.lower()
    det = detxml[:detxml.rfind('.xml')]
    for d in DETS:
        if det.find(d) >= 0:
            detcomps[d].append(det[len(d):])
#   det = det.lower()
#   dets.append(det)
print "Moving to Offline DB the following detector Data ", detcomps
ecs = EventClockSvc()
ecs.InitialTime = RunOption.RunStartTime * 1000000000
ecs.addTool(FakeEventTime, "EventTimeDecoder")
ecs.EventTimeDecoder.StartTime = ecs.InitialTime
ecs.EventTimeDecoder.TimeStep = 10
ecs.EventTimeDecoder.StartRun = RunOption.RunNumber
#xmlCnvSvc = XmlCnvSvc(AllowGenericConversion = True)
DDDBConf()
#detDataSvc = DetectorDataSvc()
#DetectorPersistencySvc( CnvServices = [ xmlCnvSvc ] )
cdb = CondDB()
cdb.RunChangeHandlerConditions = CondMap.ConditionMap
cdb.EnableRunChangeHandler = True
cdb.EnableRunStampCheck = False
cdb.UseDBSnapshot = True
cdb.Tags = {
    "DDDB": RunOption.DDDBTag,
    "LHCBCOND": RunOption.CondDbTag,
    "ONLINE": RunOption.OnlDBTag
}
cdb.IgnoreHeartBeat = True
dbxalg = DBXferAlg()
dbxalg.RunNumber = RunOption.RunNumber
dbxalg.RunStartTime = RunOption.RunStartTime * 1000000000
dbxalg.OnlineXmlDir = RunOption.OutputDirectory
dets = []
for d in detcomps.keys():
    if len(detcomps[d]) != 0:
        dets.append(d)
wrconf = WriterConf("wconf", dets)  #["Velo","IT","TT","OT"])
wrconf.CondFilePrefix = RunOption.OutputDirectory + "/offl/Conditions/Online"
from Configurables import TAlignment, WriteMultiAlignmentConditions
talign = TAlignment()  # used for properties
xmlWriters = WriteMultiAlignmentConditions()
xmlWriters.addXmlWriters(talign, detcomps)
print detcomps
print xmlWriters.XmlWriters
app = ApplicationMgr()
app.TopAlg = [dbxalg]
#app.ExtSvc += [ detDataSvc]#, rch ]
app.EvtSel = "NONE"
app.EvtMax = 1
app.OutputLevel = INFO
RunStart = HLT2Params.RunStartTime * 1000000000
RunEnd = (HLT2Params.RunStartTime + HLT2Params.RunDuration) * 1000000000
print args
if len(args) > 1:
    lstrun = args[1]
    if lstrun == 'LastRun':
        RunEnd = None
# from Configurables import XmlParserSvc
#RunChangeHandlerSvc(OutputLevel=VERBOSE)
#MessageSvc(OutputLevel=1)
EventDataSvc(ForceLeaves=True)
from GaudiPython.Bindings import AppMgr
Gaudi = AppMgr()
print "===================== Initializing the XML conversion ========================"
stat = Gaudi.initialize()
print "============= Status :", stat
if stat.isFailure():
    sys.exit(44)
print "============= Gaudi.ReturnCode :", Gaudi.ReturnCode
if Gaudi.ReturnCode != 0:
    sys.exit(44)
print "===================== Running the XML conversion ========================"
stat = Gaudi.run(1)
print "============= Status :", stat
if stat.isFailure():
    sys.exit(44)
print "============= Gaudi.ReturnCode :", Gaudi.ReturnCode
if Gaudi.ReturnCode != 0:
    sys.exit(44)
print "===================== Stopping the XML conversion ========================"
stat = Gaudi.stop()
print "============= Status :", stat
if stat.isFailure():
    sys.exit(44)
print "============= Gaudi.ReturnCode :", Gaudi.ReturnCode
if Gaudi.ReturnCode != 0:
    sys.exit(44)
print "===================== Finalizing the XML conversion ========================"
stat = Gaudi.finalize()
xmlWriters.writeAlignmentConditions(dbxalg)
print "============= Status :", stat
if stat.isFailure():
    sys.exit(44)
print "============= Gaudi.ReturnCode :", Gaudi.ReturnCode
if Gaudi.ReturnCode != 0:
    sys.exit(44)
print "===================== Exiting the XML conversion ========================"
stat = Gaudi.exit()
print "============= Status :", stat
if stat.isFailure():
    sys.exit(44)
print "===================== Updating the Database ========================"
import CondDBUI
import CondDBUI.Admin
DBString = "CondDBOnline(owner)/ONLINE"  # use the central Oracle Database...
####DBString = "sqlite_file:./CONDDB.db/ONLINE"
db = CondDBUI.CondDB(DBString, create_new_db=False, readOnly=False)
import time
if RunEnd == None:
    etime = HLT2Params.RunStartTime + 86400 * 365 * 100
else:
    etime = RunEnd / 1000000000
print "Operating on Database " + DBString
print "IoV is [" + str(RunStart) + "," + str(RunEnd) + "], [" + time.ctime(
    HLT2Params.RunStartTime) + "," + time.ctime(etime) + "]"
status = 0
try:
    status = CondDBUI.Admin.MakeDBFromFiles(
        RunOption.OutputDirectory + "/offl",
        db,
        includes=[],
        excludes=[],
        verbose=True,
        since=RunStart,
        until=RunEnd,
        writeDuplicate=False)
except Exception as inst:
    print "Exception during moving files to Database"
    print inst.message
    print inst.args
    exit(43)
# GITCONDDB_DIR = '/group/online/hlt/conditions/git-conddb/ONLINE'
GITCONDDB_DIR = '/git/git-conddb/ONLINE'
print "Updating GIT-based Database"
try:
    add_files_to_gitconddb.git_conddb_extend(
        RunOption.OutputDirectory + "/offl",
        GITCONDDB_DIR,
        since=RunStart,
        until=RunEnd)
except Exception as e:
    print 'Exception during copy to Git:', e
    exit(43)

# RunEnd =  (HLT2Params.RunStartTime+HLT2Params.RunDuration)*1000000000
# print "Reopening Database in Read-Only mode"
# db.closeDatabase()
# db = CondDBUI.CondDB(DBString, create_new_db = False, readOnly=True)
# print "Checking Database Contents against entered files"
# Fails = CondDBUI.Admin.CompareDBToFiles(RunOption.OutputDirectory+"/offl", db,
#                                    includes = [], excludes = [],
#                                    verbose = True,
#                                    since = RunStart,
#                                    until = RunEnd
#                                     )
# from pprint import pprint
# if Fails:
#   print "Readback check on DB failed\n"
#   pprint(Fails)
#   sys.exit(44)
print "Creating the Run Tick File for run " + str(RunOption.RunNumber)
MakeTick(RunOption.OutputDirectory, RunOption.RunNumber)
RunEnd = (HLT2Params.RunStartTime + HLT2Params.RunDuration) * 1000000000
print "Creating the 'Tranfer_done' File for run " + str(RunOption.RunNumber)
db_doneFile = open(RunOption.OutputDirectory + "/../CondDBXfer_done", "w")
db_doneFile.write(
    str(HLT2Params.RunStartTime) + " " + str(HLT2Params.RunDuration) + "\n")
db_doneFile.close()
print "NOT Adding the RunTick to the Database"
# print "IoV is ["+str(RunStart)+","+str(RunEnd)+"]"+", ["+time.ctime(HLT2Params.RunStartTime)+","+time.ctime(RunEnd/1000000000)+"]"
# db = CondDBUI.CondDB(DBString, create_new_db = False, readOnly=False)
# status = CondDBUI.Admin.MakeDBFromFiles(RunOption.OutputDirectory+"/onl", db,
#                                    includes = [], excludes = [],
#                                    verbose = True,
#                                    since = RunStart,
#                                    until = RunEnd,
#                                    writeDuplicate = False
#                                     )
# db.closeDatabase()
# db = CondDBUI.CondDB(DBString, create_new_db = False, readOnly=True)
# print "Checking the Run Tick"
# Fails = CondDBUI.Admin.CompareDBToFiles(RunOption.OutputDirectory+"/onl", db,
#                                    includes = [], excludes = [],
#                                    verbose = True,
#                                    since = RunStart,
#                                    until = RunEnd
#                                     )
# if Fails:
#   print "Readback check on DB failed\n"
#   pprint(Fails)
#   sys.exit(44)
# db.closeDatabase()
# DBString = "CondDBOnline/ONLINE" # use the central Oracle Database...
# db = CondDBUI.CondDB(DBString, create_new_db = False, readOnly=True)
# print "Checking Database Contents against entered files with account "+DBString
# Fails = CondDBUI.Admin.CompareDBToFiles(RunOption.OutputDirectory+"/offl", db,
#                                    includes = [], excludes = [],
#                                    verbose = True,
#                                    since = RunStart,
#                                    until = RunEnd
#                                     )
# if Fails:
#   print "Readback check on DB failed\n"
#   pprint(Fails)
#   sys.exit(44)
# print "Checking The Run Tick contents with account "+DBString
# Fails = CondDBUI.Admin.CompareDBToFiles(RunOption.OutputDirectory+"/onl", db,
#                                    includes = [], excludes = [],
#                                    verbose = True,
#                                    since = RunStart,
#                                    until = RunEnd
#                                     )
# if Fails:
#   print "Readback check on DB failed\n"
#   pprint(Fails)
#   sys.exit(44)

print "===================== Updated the Database ======================== Status = ", status

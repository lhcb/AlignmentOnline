/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * hr.h
 *
 *  Created on: Jul 21, 2015
 *      Author: beat
 */

#ifndef SOURCE_DIRECTORY__ALIGNMENTONLINE_ALIGNONLINE_SRC_HISTOGRAMRESETTER_H_
#define SOURCE_DIRECTORY__ALIGNMENTONLINE_ALIGNONLINE_SRC_HISTOGRAMRESETTER_H_

#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"

class HistogramResetter : public GaudiHistoAlg, virtual public IIncidentListener {
public:
  HistogramResetter( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~HistogramResetter(); ///< Destructor
  virtual StatusCode initialize() override;
  virtual StatusCode finalize() override; ///< Algorithm initialization
  virtual StatusCode execute() override;  ///< Algorithm execution
  virtual StatusCode stop() override;     ///< Transition executed upon the Stop command
  virtual StatusCode start() override;    ///< Transition executed upon the Start command
  /// virtuals incident
  void       handle( const Incident& ) override;
  StatusCode queryInterface( const InterfaceID& id, void** ppI ) override;
};

#endif /* SOURCE_DIRECTORY__ALIGNMENTONLINE_ALIGNONLINE_SRC_HISTOGRAMRESETTER_H_ */

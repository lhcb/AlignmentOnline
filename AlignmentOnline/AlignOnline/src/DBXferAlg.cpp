/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * AligOnlIterator.cpp
 *
 *  Created on: Oct 6, 2014
 *      Author: beat
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/Time.h"

#include "RTL/rtl.h"

#include <boost/filesystem.hpp>

#include "DetDesc/RunChangeIncident.h"
#include "GaudiKernel/IDetDataSvc.h"
#include "GaudiKernel/IIncidentSvc.h"

class DBXferAlg : public GaudiAlgorithm {
public:
  DBXferAlg( const std::string& name, ISvcLocator* svcloc );
  StatusCode initialize() override;
  StatusCode stop() override;
  StatusCode execute() override;

private:
  std::string            m_onlinexmldir;
  std::string            m_alignxmldir;
  Gaudi::Time::ValueType m_RunStartTime;
  int                    m_RunNumber;
};

DECLARE_COMPONENT( DBXferAlg )

DBXferAlg::DBXferAlg( const std::string& name, ISvcLocator* svcloc ) : GaudiAlgorithm( name, svcloc ) {
  declareProperty( "OnlineXmlDir", m_onlinexmldir = "/group/online/alignment" );
  declareProperty( "RunStartTime", m_RunStartTime );
  declareProperty( "RunNumber", m_RunNumber );
}

StatusCode DBXferAlg::initialize() {
  //	sleep(20);
  printf( "Initializing DBXferAlg...\n" );
  return GaudiAlgorithm::initialize();
}

StatusCode DBXferAlg::execute() {
  printf( "DBXferAlg Execute called...\n" );
  StatusCode sc = StatusCode::SUCCESS;
  // only called once

  //  int runnr = 0;
  IDetDataSvc* detDataSvc( 0 );
  sc = service( "DetectorDataSvc", detDataSvc, false );
  if ( sc.isFailure() ) {
    //    error() << "Could not retrieve DetectorDataSvc" << endmsg;
    return sc;
  }
  IIncidentSvc* incSvc;
  sc = service( "IncidentSvc", incSvc, false );

  // 4. read ASDs and compute new constants
  printf( "DBXferAlg Fireing RunChange incident...\n" );

  detDataSvc->setEventTime( m_RunStartTime );
  incSvc->fireIncident( RunChangeIncident( name(), m_RunNumber ) );
  sc = updMgrSvc()->newEvent();
  if ( !sc ) { printf( "DBXferAlg Bad return from the update service\n" ); }

  return sc;
}

StatusCode DBXferAlg::stop() {
  // called only once
  return StatusCode::SUCCESS;
}

// StatusCode DBXferAlg::queryInterface(const InterfaceID& riid,
//    void** ppvIF)
//{
//  if (LHCb::IAlignIterator::interfaceID().versionMatch(riid))
//  {
//    *ppvIF = (IAlignIterator*) this;
//    addRef();
//    return StatusCode::SUCCESS;
//  }
//  return GaudiTool::queryInterface(riid, ppvIF);
//}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * ASDCollector.h
 *
 *  Created on: Oct 6, 2014
 *      Author: beat
 */

#ifndef ASDCOLLECTOR_H_
#define ASDCOLLECTOR_H_
#include "AlignKernel/AlEquations.h"
#include "time.h"
#include <string>
#include <vector>

class ASDCollector {
public:
  std::string m_dir;
  std::string m_filePatt;
  time_t      m_time;
  ASDCollector() {}
  ASDCollector( std::string& FilePatt, std::string& dir );
  void   setTime( time_t t );
  void   setTime();
  size_t collectASDs( LHCb::Alignment::Equations& eqs );
  int    getfiles( std::string dir, std::vector<std::string>& fileList );
  int    isDir( std::string path );
};

#endif /* ASDCOLLECTOR_H_ */

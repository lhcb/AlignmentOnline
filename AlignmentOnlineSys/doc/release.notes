<PRE>
Package: AlignmentOnlineSys
Package Coordinator: Maurizio Martinelli
Purpose: Running and testing Alignment in the online environment


<H1>2017-06-30 AlignmentOnlineSys v11r4</H1>
This version is intended for 2017 data taking. Relies on the latest version to date of Online (v6r5) and Alignment (v11r4p1)

Merge requests included:
 - !25 @pnaik: Updated condbChecker for new online release
 - !24 @pnaik: Included GaudiKernel/IPublishSvc.h in Calibration/OTOnlineCalibration/src/OTt0OnlineClbr.h
 - !22 @mamartin: added a cmake option to find XQilla dependency

Commits added directly after cherry-picking from satellite
 - b8df07a796b051ba988c10710366dbaa52c427d6 @mamartin: Updated to match most recent Online changes
 - 24780254a16b376560fbd2db552cdbfaf2ed9ec1 @gdujany: read from good git DB
 - 4bfc70af57c7a11185d97e1ba1129abd6752e978 @fdordei: Bug fix: version was send online in case of one or less iteration triggering an alarm
 - 7dc6a2f5995060bf0717b9a99118501985b5a521 @fdordei: Bug fix: version was send online in case of one or less iteration triggering an alarm
 - 9b0701847896fc6b9ea8ecfbc27cdf884a4f90d8 @gdujany: updated check constants
 - e1846144eee431b5c29a3ff6b96ba5d2c885f9ac @lgrillo: correctly reading the conditions for 2017
 - f98a2e83941f21b7d0be82a6648dd3ab3d38c72e @lgrillo: read run time from the conditions in the run directory, force RunChangeHandler to update the conditions, use git DB, other configuration to read the conditions
 - 584d2317a8fee42a408ca065d3feccf7c860a312 @lgrillo: fix in xml parsing in OTt0OnlineClbr


<H1>2017-04-10 AlignmentOnlineSys v11r3</H1>
This version is intended for 2017 commissioning. Relies on the latest version to date of Online (v6r3) and Alignment (v11r3)
Relese triggered by the update of Online. Further modifications applied after the start of commissioning:
 - Extended DBXfer* to update Git CondDB !16
 - Update CondbCheck for 2017 conditions and git database !17

<H1>2017-03-14 AlignmentOnlineSys v11r2</H1>
This version is intended for 2017 commissioning. Relies on the latest version to date of Online (v6r1) and Alignment (v11r2)
To release the gcc62 version of the project a few modifications are applied to fix warnings:
 - See !10 !11 !12

<H1>2017-02-24 AlignmentOnlineSys v11r1</H1>
This version is intended for 2017 commissioning. Relies on the latest version to date of Online (v6r1) and Alignment (v11r1p1)

First git release of the project including the following packages:
 - AlignmentOnline/AlignOnline
 - AlignmentOnline/PyGeneticOnline
 - AlignmentOnline/PyKaliOnline
 - Calibration/OTOnlineCalibration

Merge requests included:
   - @mamartin: fixed a few compilation warnings !6.
   - @raaij: Fix the build with the head of online !5.
   - @gdujany: more info to the dim service in case of big variations !4.
   - @gdujany: Alarms tracker !2.

Older SVN commits:
   - @dsavrina: updated the tags and software versions for the 2016 data [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/1ba0f7c8ce4e8fa21e970afe79cd200c1463598a)
   - @lgrillo: MaxDifference threshold set to 1ns (was 2ns) [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/3b1af26ab81845aee6d76912e1513956730ad0b6)
   - @gdujany: Changed alarm to call Alignment piquet instead of HLT one [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/dcfbfa4bd4e79af314ba479719a4f38c5a83ee59)
   - @gdujany: Added to call the HLT piquet in case of problem in automatic alignment in the error message [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/bc1f110ab6bd79116781685acbdaec9872a2c284)
   - @lgrillo: bug fix in publishing services OTCalib [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/1b427dda817d06ea58efe29835e502957e1299ae)
   - @gdujany: Added scriptto makelocal CALIBOFF database from official alignment xmls [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/236a365c53bbb7e87d004264e2ded72ba77a8719)
   - @lgrillo: added another service to publish t0, dt0, dt0err [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/e80a66ca806effff10de6114b38a3c94d04b5670)
   - @fdordei: MsgStream added to the PrintWarning function in the iterator. [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/6cc9f6b80faa7e14d1215c594411afe4a09f76cf)
   - @lgrillo: Alarm messages for the OT t0 calibration [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/871a8e797b464a9a68907748470fb2047e897d3e)
   - @gdujany: improvements and bug fixes for 2016 restart [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/e0800723d75573a90a076622beb3b2d4e90028e8)
   - @dsavrina: Multiprocessing for fmDST production, histograms merging moved to Analyser [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/263d909f73677f8f27fd3622b07a69cb893b64a4)
   - @mamartin: fixed a bug in previous commit [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/68f44468c8b7f3b64fad90f8776b34706743da20)
   - @mamartin: added version number to runnumber directory where alignment results are saved [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/c9d04ac134d1dedad968dd289d52bbd4f0eb9b30)
   - @lgrillo: use LazyRunable, use 2016 condition, add alarms [link](https://gitlab.cern.ch/lhcb/AlignmentOnline/commit/40c10f8f08f0598691cd78c79ab6f60a532336b7)

</PRE><H1><A NAME=v11r0>2016-04-12 AlignmentOnlineSys v11r0</A></H1><PRE>
Released the project with the following new tags:
   https://lhcb-tag-collector.web.cern.ch/lhcb-tag-collector/display.html?project=AlignmentOnline&version=v11r0
 - This version is intended for 2016 data-taking. Relies on the latest version to date of Online (v5r31) and Alignment (v11r0)

</PRE><H1><A NAME=v10r6>2016-03-03 AlignmentOnlineSys v10r6</A></H1><PRE>
Released the project with the following new tags:
   https://lhcb-tag-collector.web.cern.ch/lhcb-tag-collector/display.html?project=AlignmentOnline&version=v10r6

! 2016-03-03 - Maurizio Martinelli
 - Updated refereces for qmtest

! 2016-03-01 - Maurizio Martinelli
 - Updated dependencies on tagged package on view of the forthcoming release

! 2015-08-25 - Maurizio Martinelli
 - Added dependency on Calibration/OTOnlineCalibration
 - Added dependency on AlignmentOnline/PyGeneticOnline

</PRE><H1><A NAME=v10r5>2015-07-30 AlignmentOnlineSys v10r5</A></H1><PRE>
Released the project with the following new tags:
   https://lhcb-tag-collector.web.cern.ch/lhcb-tag-collector/display.html?project=AlignmentOnline&version=v10r5

</PRE><H1><A NAME=v10r4p1>2015-06-10 AlignmentOnlineSys v10r4p1</A></H1><PRE>
Released the project with the following new tags:
   https://lhcb-tag-collector.web.cern.ch/lhcb-tag-collector/display.html?project=AlignmentOnline&version=v10r4p1

! 2015-06-10 - Maurizio Martinelli
 - Removed unnecessary dependencies

</PRE><H1><A NAME=v10r4>2015-06-04 AlignmentOnlineSys v10r4</A></H1><PRE>
Released the project with the following new tags:
   https://lhcb-tag-collector.web.cern.ch/lhcb-tag-collector/display.html?project=AlignmentOnline&version=v10r4

! 2015-06-04 - Maurizio Martinelli
 - Fixed QMtest reference

</PRE><H1><A NAME=v10r3>2015-04-07 AlignmentOnlineSys v10r3</A></H1><PRE>
Released the project with the following new tags:
   https://lhcb-tag-collector.web.cern.ch/lhcb-tag-collector/display.html?project=AlignmentOnline&version=v10r3

Follows update of Alignment to v10r3.
Includes modules for configuring specific alignment scenarios.


</PRE><H1><A NAME=v10r2>2015-01-23 AlignmentOnlineSys v10r2</A></H1><PRE>
Released the project with the following new tags:
	 https://lhcb-tag-collector.web.cern.ch/lhcb-tag-collector/display.html?project=AlignmentOnline&version=v10r2

Most significative improvement is the possibility to run Alignment tests on one node of hte Hlt farm.

! 2014-12-17 - Roel Aaij
 - Add use statements for OMAlib and PRConfig

! 2014-12-17 - Maurizio Martinelli
 - Fixed QMtest reference

</PRE><H1><A NAME=v10r0>2014-11-10 AlignmentOnlineSys v10r1</A></H1><PRE>
First release of the Online wrapper project for Alignment. AlignmentOnline has the same version as the underlying Alignment version for simplicity/transparency.

It's a project which can be used in the nightlies to test that running Alignment works in the online environment. It can also hold online-specific code. All the online-specific parts of Alignment fall into this project.

Only package so far: AlignOnline v1r0 to hold the relevant interface code.
